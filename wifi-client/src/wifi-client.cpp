#include "ESP8266HTTPClientPOCFork.h"
#include <ESP8266WiFi.h>

const char* ssid = "SENSOR_SERVER";
const char* password = "UltimatePW";

//Your IP address or domain name with URL path
const char* serverCount = "http://192.168.1.200/count-of-requests";

unsigned long previousMillis = 0;
const long interval = 5000; 



std::string httpGETRequest(const char* serverName) {
  HTTPClient http;
  http.setTimeout(2000);
  http.begin(serverName);
  Serial.print("call ");
  Serial.println(serverName);
  // Send HTTP POST request
  int httpResponseCode = http.GET();
  
  std::string payload = "-NO_PAYLOAD-"; 
  
  if (httpResponseCode > 0) {
    Serial.print("HTTP Response code: ");
    Serial.println(httpResponseCode);
    payload = http.getString();
  }
  else {
    Serial.print("Error code: ");
    Serial.println(httpResponseCode);
  }
  http.end();
  return payload;
}

void setup() {
  Serial.begin(9600);
  
  WiFi.begin(ssid, password);
  Serial.println("Connecting");
  while(WiFi.status() != WL_CONNECTED) { 
    delay(500);
    Serial.print(".");
  }
  Serial.println("");
  Serial.print("Connected to WiFi network with IP Address: ");
  Serial.println(WiFi.localIP());
}

void loop() {
  unsigned long currentMillis = millis();
  
  if(currentMillis - previousMillis >= interval) {
     // Check WiFi connection status
    if(WiFi.status()== WL_CONNECTED ){ 
      std::string response = httpGETRequest(serverCount);
      Serial.print("Server: ");
      Serial.println(response.c_str());
      
      // save the last HTTP GET Request
      previousMillis = currentMillis;
    }
    else {
      Serial.println("WiFi Disconnected");
    }
  }
}
