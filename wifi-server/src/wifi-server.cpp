
#include <ESP8266WiFi.h>
#include <ESPAsyncWebServer.h>


const char* ssid = "SENSOR_SERVER";
const char* password = "UltimatePW";

IPAddress ip(192,168,1,200);
IPAddress gateway(192,168,1,254);
IPAddress subnet(255,255,255,0);

AsyncWebServer server(80);
int requestCount;


char* getCount(){
  requestCount++;
  String payload = "Count: " + String(requestCount);
  char charArray[payload.length() + 1];
  payload.toCharArray(charArray, payload.length() + 1);
  Serial.println(charArray);
  return charArray;
}

void setup(){
  Serial.begin(9600);
  
  Serial.print("Setting AP (Access Point)…");
  WiFi.softAPConfig(ip, gateway, subnet);
  WiFi.softAP(ssid, password);
  IPAddress IP = WiFi.softAPIP();
  
  Serial.print("AP IP address: ");
  Serial.println(IP);

  server.on("/count-of-requests", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", getCount());
  });
  server.on("/health", HTTP_GET, [](AsyncWebServerRequest *request){
    request->send_P(200, "text/plain", "Wifi server is alive!");
  });
  
  bool status;

  server.begin();
}
 
void loop(){
  
}